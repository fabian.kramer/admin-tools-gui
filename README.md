English:   
   
With this plugin you can manage some really important Server and world options.   
   
For example the Time of the Day, the Whitelist status and the KeepInventory function.   
   
    
   
Commands:   
   
/admintools -- Opens the GUI   
        /at -- shortcut for /admintools   
/at reload -- reloads the config   
/admintools server rl -- reloads the server (equal to /reload)   
/admintools server stop -- stops the server (equal to /stop)   
/servercontrol -- Direcly opens the Servercontrol panel   
/worldcontrol -- Direcly opens the Worldcontrol panel   
/spec -- Change between survival and spectatormode (only this function: SpecPlugin)   
/cc -- Clear the chat with 100 lines of nothing   
/day -- Alias for /time set day   
/night -- Alias for /time set night   
/sun -- Alias for /weather clear   
/rain -- Alias for /weather rain   
/wl <on/off> -- Alias for /whitelist   
/gr <true/false> -- Shortform for doDaylightCycle = "dc", doMobSpawning = "ms", commandBlockOutput = "cbo" and so on   
   
   
Permissions:   
admintools.admin -- permits anything (including restarting and stopping the server)   
admintools.open -- open GUIs   
admintools.spec -- use /spec   
admintools.cc -- permits to clear chat   
admintools.wl -- permits whitelist features   
admintools.time -- permits time features   
admintools.weather -- permits weather features   
admintools.gr -- perimits to change all gamerules   
    
   
The Permissions not only grant usage of the commands it also grants usage of the corosponding GUI features.   

   
GUI:      
Sortet in Server category and World category.   
-Server:   
  Whitelist:   
    on   
    off   
    list (deactivated)   
  Server stop   
  Server reload   
  Server Stats -- requre ServerStats plugin (Link: *click*)   
    
   
-World:   
  Night   
  Day   
  Rain   
  Sun   
  DaylightCycle on/off   
  KeepInventory on/off   
  ChatClear   
  doMobspawning (on/off)   
    
    
   
TO-DO list:   
√ - get Server options work   
√ - add missing World options   
√ - get all commands not depending on OP   
√ - make items move able in normal inv   
√ - refactor command structure   
X - Add new functions like Vanish, invsee, ...   
    
   
X = not done yet   
/ = maby if its possible for me   
√ = done   
► = work in progress   
    
   
Known bugs:   
- no one can move Items in Inventory permanently and everywhere -- fixed   
As of now, there are no issus / bugs known (18th of July 2019)   
    
    
   
German:   
    
   
Mit diesem Plugin kann man wichtige Server und Weltoptionen steuern.   
Zum Beispiel: Tageszeit, den Status der Whitelist und KeepInventory.   
Befehle:   
/admintools -- Öffne die GUI   
        /at -- Alias für /admintools   
/at reload -- lade die Config neu (Noch kein Inhalt)   
/admintools server rl -- lade den Server neu (wie /reload)   
/admintools server stop -- Stoppe den server (wie /stop)   
/servercontrol -- Öffnet direkt die Serveroptionen   
/worldcontrol --Öffnet direkt die Weltoptionen   
/spec -- Wechsle zwischen Spectator und Überlebensmodus (Nur diese Funktion: SpecPlugin)   
/cc -- Leere den chat mit 100 Zeilen nichts   
/day -- Alias für /time set day   
/night -- Alias für /time set night   
/sun -- Alias für /weather clear   
/rain -- Alias für /weather rain   
/wl <on/off> -- Alias für /whitelist   
/gr <true/false> -- Kurtzform für doDaylightCycle = "dc", doMobSpawning = "ms", commandBlockOutput = "cbo" und so weiter   
    
   
Permissions:   
admintools.admin -- Erlaubt jeden Befehl (Inclusive stoppen und Starten des Servers)   
admintools.open -- GUIs öffnen   
admintools.spec -- /spec verwenden   
admintools.cc -- Clear Chat Funktionen   
admintools.wl -- Whitelist an und aus schalten   
admintools.time -- Die Zeit auf Tag und Nacht setzen   
admintool.weather -- Wetter auf Sonne oder Regen stellen   
admintools.gr -- Gamerule Funktionen   
   
Die Permissions berechtigen sowohl für die GUI Funktionen als auch die entsprechenden Befehle.   