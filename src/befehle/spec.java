package befehle;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.funky33.admintools.Main;
import de.funky33.admintools.MinecraftCmd;

public class spec extends MinecraftCmd {
	
	public ArrayList<String> specPlayers =new ArrayList<String>();
	
	public spec() {
		super("spec");
	}
	
	public void doIt(CommandSender sender, Command cmd, String label, String[] args, String prefix) {
		Player player = (Player)sender;
		String no_permission = Main.getInstance().getConfig().getString("system.no_permission");
    	String no_permission_spec = no_permission.replace("[Permission]", "admintools.spec");
    	
		if (sender instanceof Player) {
			if (cmd.getName().equalsIgnoreCase("spec")) {
				if (player.hasPermission("admintools.spec") | player.hasPermission("admintools.admin"))	{
					if (args.length == 0) {
						if (specPlayers.contains(player.getName()))	{
							player.setGameMode(GameMode.SURVIVAL);
							specPlayers.remove(player.getName());
							player.sendMessage(prefix + "Du bist kein Spectator mehr!");
						} else {
							player.setGameMode(GameMode.SPECTATOR);
							specPlayers.add(player.getName());
							player.sendMessage(prefix + "Du bist jetzt Spectator!");
						}
					} else if (args.length > 0) {
						player.sendMessage(prefix + ChatColor.RED + "Usage: /spec");
					}
				} else {
					player.sendMessage(prefix + ChatColor.translateAlternateColorCodes('&', no_permission_spec));
				}
			}
		} else {
			sender.sendMessage(prefix + ChatColor.RED + "You have to be a player to use this command!");
		}
	}
}
