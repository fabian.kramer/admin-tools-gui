package befehle;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.funky33.admintools.Main;
import de.funky33.admintools.MinecraftCmd;

/**
 *
 * @author funky33
 */
public class admintools extends MinecraftCmd implements Listener {
    
    public admintools() {
        super("admintools"); //Command in Chat
    }

    @Override
    public void doIt(CommandSender sender, Command cmd, String label, String[] args, String prefix) { //What will be executed
    	Player player = (Player)sender;
    	String no_permission = Main.getInstance().getConfig().getString("system.no_permission");
    	String no_permission_admin = no_permission.replace("[Permission]", "admintools.admin");
    	String no_permission_open = no_permission.replace("[Permission]", "admintools.open");
    	
		// GUI
		if (cmd.getName().equalsIgnoreCase("admintools")) {
			if (args.length < 1) {
				if (player.hasPermission("admintools.open") | player.hasPermission("admintools.admin"))	{
					openGUI(player.getPlayer());
				} else {
					player.sendMessage(prefix + ChatColor.translateAlternateColorCodes('&', no_permission_open));
				}
			}
			if (args.length == 2 && args[0].equalsIgnoreCase("server") && player.hasPermission("admintools.admin")) {
				if (args[1].equalsIgnoreCase("rl"))
					Bukkit.reload();
					player.sendMessage(prefix + ChatColor.GREEN + "Server reloaded. " + ChatColor.RED + "Remember, a reload does not equal a restart.");
				if (args[1].equalsIgnoreCase("stop"))
					Bukkit.shutdown();
			} else if (args.length == 2) {
				if (!player.hasPermission("admintools.admin")) {
					player.sendMessage(prefix + ChatColor.translateAlternateColorCodes('&', no_permission_admin));
				} else {
					player.sendMessage(prefix + ChatColor.RED + "Usage: /at server <rl/stop>");
				}
			}
		}
    }
    
    
    // GUI
 	public void openGUI(Player player) {
 		Inventory inv = Bukkit.createInventory(null, 9, ChatColor.GREEN + "Admin Tools");

 		ItemStack control = new ItemStack(Material.COMMAND_BLOCK_MINECART);
 		ItemMeta controlMeta = control.getItemMeta();
 		ItemStack world = new ItemStack(Material.GRASS);
 		ItemMeta worldMeta = world.getItemMeta();
 		ItemStack close = new ItemStack(Material.BARRIER, 1);
 		ItemMeta closemeta = close.getItemMeta();
 		ItemStack back = new ItemStack(Material.ARROW, 1);
 		ItemMeta backmeta = back.getItemMeta();
 		ItemStack filler = new ItemStack(Material.GRAY_STAINED_GLASS_PANE, 1);
 		ItemMeta fillermeta = filler.getItemMeta();

 		fillermeta.setDisplayName("null");
 		filler.setItemMeta(fillermeta);

 		backmeta.setDisplayName("Back");
 		back.setItemMeta(backmeta);

 		closemeta.setDisplayName("Close");
 		close.setItemMeta(closemeta);

 		controlMeta.setDisplayName("Server Control");
 		control.setItemMeta(controlMeta);

 		worldMeta.setDisplayName("World options");
 		world.setItemMeta(worldMeta);

 		inv.setItem(0, filler);
 		inv.setItem(1, filler);
 		inv.setItem(2, control);
 		inv.setItem(3, filler);
 		inv.setItem(4, filler);
 		inv.setItem(5, filler);
 		inv.setItem(6, world);
 		inv.setItem(7, filler);
 		inv.setItem(8, close);

 		player.openInventory(inv);
 	}
 	
}
