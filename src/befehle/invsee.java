package befehle;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.funky33.admintools.Main;
import de.funky33.admintools.MinecraftCmd;

public class invsee extends MinecraftCmd{
	public invsee() {
		super("invsee");
	}

	@Override
	public void doIt(CommandSender sender, Command cmd, String label, String[] args, String prefix) {
		Player player = (Player)sender;
		String no_permission = Main.getInstance().getConfig().getString("system.no_permission");
    	String no_permission_invsee = no_permission.replace("[Permission]", "admintools.invsee");
    	
		if (cmd.getName().equalsIgnoreCase("invsee")) {
			if (player.hasPermission("admintools.invsee") || player.hasPermission("admintools.admin")) {
				if (args.length == 1) {
					Player target = Bukkit.getPlayer(args[0]);
				    player.openInventory(target.getInventory());
				} else {
					player.sendMessage(prefix + ChatColor.RED + "Usage: /invsee <player>");
				}
			} else {
				player.sendMessage(prefix + ChatColor.translateAlternateColorCodes('&', no_permission_invsee));
			}
		}
	}
}
