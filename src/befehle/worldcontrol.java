package befehle;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.funky33.admintools.Main;
import de.funky33.admintools.MinecraftCmd;

public class worldcontrol extends MinecraftCmd implements Listener {
	public worldcontrol() {
		super("worldcontrol");
	}
	
	public void doIt(CommandSender sender, Command cmd, String label, String[] args, String prefix) {
		Player player = (Player)sender;
		String no_permission = Main.getInstance().getConfig().getString("system.no_permission");
    	String no_permission_open = no_permission.replace("[Permission]", "admintools.open");
    	
		if (cmd.getName().equalsIgnoreCase("worldcontrol")) {
			if (player.hasPermission("admintools.open") | player.hasPermission("admintools.admin")) {
				openWorld(player.getPlayer());
			} else {
				player.sendMessage(prefix + ChatColor.translateAlternateColorCodes('&', no_permission_open));
			}
		}
	}
	
	public void openWorld(Player player) {
		Inventory inv = Bukkit.createInventory(null, 9*3, "Admin Tools / World");

		ItemStack day = new ItemStack(Material.WHITE_WOOL);
		ItemMeta daymeta = day.getItemMeta();
		ItemStack night = new ItemStack(Material.BLACK_WOOL, 1);
		ItemMeta nightmeta = night.getItemMeta();
		ItemStack close = new ItemStack(Material.BARRIER, 1);
		ItemMeta closemeta = close.getItemMeta();
		ItemStack back = new ItemStack(Material.ARROW, 1);
		ItemMeta backmeta = back.getItemMeta();
		ItemStack sun = new ItemStack(Material.GLOWSTONE, 1);
		ItemMeta sunmeta = sun.getItemMeta();
		ItemStack rain = new ItemStack(Material.WATER_BUCKET, 1);
		ItemMeta rainmeta = rain.getItemMeta();
		ItemStack daycy = new ItemStack(Material.REDSTONE_LAMP, 1);
		ItemMeta daycymeta = daycy.getItemMeta();
		ItemStack kinv = new ItemStack(Material.CHEST, 1);
		ItemMeta kinvmeta = kinv.getItemMeta();
		ItemStack mspa = new ItemStack(Material.ZOMBIE_SPAWN_EGG, 1);
		ItemMeta mspameta = mspa.getItemMeta();
		ItemStack cc = new ItemStack(Material.WRITABLE_BOOK, 1);
		ItemMeta ccmeta = cc.getItemMeta();
		ItemStack daycyt = new ItemStack(Material.GREEN_STAINED_GLASS_PANE, 1);
		ItemMeta daycytmeta = daycyt.getItemMeta();
		ItemStack daycyf = new ItemStack(Material.RED_STAINED_GLASS_PANE, 1);
		ItemMeta daycyfmeta = daycyf.getItemMeta();
		ItemStack kinvt = new ItemStack(Material.GREEN_STAINED_GLASS_PANE, 1);
		ItemMeta kinvtmeta = kinvt.getItemMeta();
		ItemStack kinvf = new ItemStack(Material.RED_STAINED_GLASS_PANE, 1);
		ItemMeta kinvfmeta = kinvf.getItemMeta();
		ItemStack mspat = new ItemStack(Material.GREEN_STAINED_GLASS_PANE, 1);
		ItemMeta mspatmeta = mspat.getItemMeta();
		ItemStack mspaf = new ItemStack(Material.RED_STAINED_GLASS_PANE, 1);
		ItemMeta mspafmeta = mspaf.getItemMeta();
		ItemStack filler = new ItemStack(Material.GRAY_STAINED_GLASS_PANE, 1);
		ItemMeta fillermeta = filler.getItemMeta();

		fillermeta.setDisplayName("null");
		filler.setItemMeta(fillermeta);

		daycytmeta.setDisplayName("doDaylightCycle true");
		daycyt.setItemMeta(daycytmeta);

		daycyfmeta.setDisplayName("doDaylightCycle false");
		daycyf.setItemMeta(daycyfmeta);

		mspatmeta.setDisplayName("doMobSpawning true");
		mspat.setItemMeta(mspatmeta);

		mspafmeta.setDisplayName("doMobSpawning false");
		mspaf.setItemMeta(mspafmeta);

		kinvtmeta.setDisplayName("KeepInventory true");
		kinvt.setItemMeta(kinvtmeta);

		kinvfmeta.setDisplayName("KeepInventory false");
		kinvf.setItemMeta(kinvfmeta);

		daycymeta.setDisplayName("DaylightCycle");
		daycy.setItemMeta(daycymeta);

		kinvmeta.setDisplayName("KeepInventory");
		kinv.setItemMeta(kinvmeta);

		mspameta.setDisplayName("MobSpawning");
		mspa.setItemMeta(mspameta);

		ccmeta.setDisplayName("Clear the Chat");
		cc.setItemMeta(ccmeta);

		rainmeta.setDisplayName("Turn Rain on");
		rain.setItemMeta(rainmeta);

		sunmeta.setDisplayName("Turn weather to clear");
		sun.setItemMeta(sunmeta);

		backmeta.setDisplayName("Back");
		back.setItemMeta(backmeta);

		closemeta.setDisplayName("Close");
		close.setItemMeta(closemeta);

		daymeta.setDisplayName("Day");
		day.setItemMeta(daymeta);

		nightmeta.setDisplayName("Night");
		night.setItemMeta(nightmeta);

		inv.setItem(0, day);
		inv.setItem(1, night);
		inv.setItem(2, filler);
		inv.setItem(3, sun);
		inv.setItem(4, rain);
		inv.setItem(5, filler);
		inv.setItem(6, daycy);
		inv.setItem(7, daycyt);
		inv.setItem(8, daycyf);
		inv.setItem(9, filler);
		inv.setItem(10, filler);
		inv.setItem(11, filler);
		inv.setItem(12, filler);
		inv.setItem(13, filler);
		inv.setItem(14, filler);
		inv.setItem(15, kinv);
		inv.setItem(16, kinvt);
		inv.setItem(17, kinvf);
		inv.setItem(18, mspa);
		inv.setItem(19, mspat);
		inv.setItem(20, mspaf);
		inv.setItem(21, filler);
		inv.setItem(22, cc);
		inv.setItem(23, filler);
		inv.setItem(24, filler);
		inv.setItem(25, back);
		inv.setItem(26, close);

		player.openInventory(inv);
	}
	
}
