package befehle;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.funky33.admintools.Main;
import de.funky33.admintools.MinecraftCmd;

public class cc extends MinecraftCmd {
	public cc() {
		super("cc");
	}
	
	public void doIt(CommandSender sender, Command cmd, String label, String[] args, String prefix) {
		Player player = (Player)sender;
		String no_permission = Main.getInstance().getConfig().getString("system.no_permission");
    	String no_permission_cc = no_permission.replace("[Permission]", "admintools.cc");
		
		if (sender instanceof Player) {
			if (cmd.getName().equalsIgnoreCase("cc")) {
				if (player.hasPermission("admintools.cc") | player.hasPermission("admintools.admin")) {
					if (args.length < 1) {
						for (int z = 100; z > 0; z--) {
							Bukkit.broadcastMessage("");
						}
						Bukkit.broadcastMessage(prefix + ChatColor.YELLOW + "Der Chat wurde von " + ChatColor.RESET + player.getName() + ChatColor.YELLOW + " bereinigt!");
					} else {
						try {
							Thread.sleep(5000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						int c = 100;
						for (int z = 0; c > z; c--) {
							Bukkit.broadcastMessage("");
						}
					}
				} else {
					player.sendMessage(prefix + ChatColor.translateAlternateColorCodes('&', no_permission_cc));
				}
			}
		} else {
			sender.sendMessage(prefix + ChatColor.DARK_RED + "You have to be a player!");
		}
	}
}
