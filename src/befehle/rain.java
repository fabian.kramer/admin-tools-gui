package befehle;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.funky33.admintools.Main;
import de.funky33.admintools.MinecraftCmd;

public class rain extends MinecraftCmd {
	public rain() {
		super("rain");
	}
	
	public void doIt(CommandSender sender, Command cmd, String label, String[] args, String prefix) {
		Player player = (Player)sender;
		String no_permission = Main.getInstance().getConfig().getString("system.no_permission");
    	String no_permission_weather = no_permission.replace("[Permission]", "admintools.weather");
		
		if (cmd.getName().equalsIgnoreCase("rain")) {
			if (player.hasPermission("admintools.weather")) {
				World world = player.getWorld();
				world.setStorm(true);
				player.sendMessage(prefix + "Weather changed to rain");
			} else {
				player.sendMessage(prefix + ChatColor.translateAlternateColorCodes('&', no_permission_weather));
			}
		}
	}
}
