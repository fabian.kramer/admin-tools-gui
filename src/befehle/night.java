package befehle;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.funky33.admintools.Main;
import de.funky33.admintools.MinecraftCmd;

public class night extends MinecraftCmd {
	public night() {
		super("night");
	}
	
	public void doIt(CommandSender sender, Command cmd, String label, String[] args, String prefix) {
		Player player = (Player)sender;
		String no_permission = Main.getInstance().getConfig().getString("system.no_permission");
    	String no_permission_time = no_permission.replace("[Permission]", "admintools.time");
		
		if (cmd.getName().equalsIgnoreCase("night")) {
			if (player.hasPermission("admintools.time") | player.hasPermission("admintools.admin")) {
				player.sendMessage(prefix + "Changed Time to night");
				World world = Bukkit.getWorld("world");
				world.setTime(14000);
			} else {
				player.sendMessage(prefix + ChatColor.translateAlternateColorCodes('&', no_permission_time));
			}
		}
	}
}
