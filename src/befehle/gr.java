package befehle;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.funky33.admintools.Main;
import de.funky33.admintools.MinecraftCmd;

public class gr extends MinecraftCmd {
	public gr() {
		super("gr");
	}
	
	public void doIt(CommandSender sender, Command cmd, String label, String[] args, String prefix) {
		Player player = (Player)sender;
		String no_permission = Main.getInstance().getConfig().getString("system.no_permission");
    	String no_permission_gr = no_permission.replace("[Permission]", "admintools.gr");
		
		if (cmd.getName().equalsIgnoreCase("gr")) {
			if (player.hasPermission("admintools.gr") | player.hasPermission("admintools.admin")) {

				if (args.length == 2 && args[1].matches("true|false")) {
					World world = player.getWorld();
					if (args[0].equalsIgnoreCase("cbo")) {
						world.setGameRuleValue("commandBlockOutput", args[1]);
						player.sendMessage(prefix + "Changed commandBlockOutput to " + args[1]);
					}
					if (args[0].equalsIgnoreCase("dc")) {
						world.setGameRuleValue("doDaylightCycle", args[1]);
						player.sendMessage(prefix + "Changed doDaylightCycle to " + args[1]);
					}
					if (args[0].equalsIgnoreCase("ed")) {
						world.setGameRuleValue("doEntityDrops", args[1]);
						player.sendMessage(prefix + "Changed doEntityDrops to " + args[1]);
					}
					if (args[0].equalsIgnoreCase("ft")) {
						world.setGameRuleValue("doFireTick", args[1]);
						player.sendMessage(prefix + "Changed doFireTick to " + args[1]);
					}
					if (args[0].equalsIgnoreCase("ml")) {
						world.setGameRuleValue("doMobLoot", args[1]);
						player.sendMessage(prefix + "Changed doMobLoot to " + args[1]);
					}
					if (args[0].equalsIgnoreCase("ms")) {
						world.setGameRuleValue("doMobSpawning", args[1]);
						player.sendMessage(prefix + "Changed doMobSpawning to " + args[1]);
					}
					if (args[0].equalsIgnoreCase("td")) {
						world.setGameRuleValue("doTileDrops", args[1]);
						player.sendMessage(prefix + "Changed doTileDrops to " + args[1]);
					}
					if (args[0].equalsIgnoreCase("ki")) {
						world.setGameRuleValue("keepInventory", args[1]);
						player.sendMessage(prefix + "Changed keepInventory to " + args[1]);
					}
					if (args[0].equalsIgnoreCase("lac")) {
						world.setGameRuleValue("logAdminCommands", args[1]);
						player.sendMessage(prefix + "Changed logAdminCommands to " + args[1]);
					}
					if (args[0].equalsIgnoreCase("mg")) {
						world.setGameRuleValue("mobGriefing", args[1]);
						player.sendMessage(prefix + "Changed mobGriefing to " + args[1]);
					}
					if (args[0].equalsIgnoreCase("nr")) {
						world.setGameRuleValue("naturalRegeneration", args[1]);
						player.sendMessage(prefix + "Changed naturalRegeneration to " + args[1]);
					}
					if (args[0].equalsIgnoreCase("rts")) {
						world.setGameRuleValue("randomTickSpeed", args[1]);
						player.sendMessage(prefix + "Changed randomTickSpeed to " + args[1]);
					}
					if (args[0].equalsIgnoreCase("rdi")) {
						world.setGameRuleValue("reduceDebugInfo", args[1]);
						player.sendMessage(prefix + "Changed reduceDebugInfo to " + args[1]);
					}
					if (args[0].equalsIgnoreCase("scf")) {
						world.setGameRuleValue("sendCommandFeedback", args[1]);
						player.sendMessage(prefix + "Changed sendCommandFeedback to " + args[1]);
					}
					if (args[0].equalsIgnoreCase("sdm")) {
						world.setGameRuleValue("showDeathMessages", args[1]);
						player.sendMessage(prefix + "Changed showDeathMessages to " + args[1]);
					}
				} else {
					player.sendMessage(ChatColor.RED + "Usage: /gr <shortform> <true/false> \ncommandBlockOutput=cbo \ndoDaylightCycle=dc"
							+ " \ndoEntityDrops=ed \ndoFireTick=ft \ndoMobLoot=ml \ndoMobSpawning=ms \ndoTileDrops=td \nkeepInventory=ki"
							+ " \nlogAdminCommands=lac \nmobGriefing=mg \nnaturalRegeneration=nr \nrandomTickSpeed=rts \nreduceDebugInfo=rdi"
							+ " \nsendCommandFeedback=scf \nshowDeathMessages=sdm");
				}
			} else {
				player.sendMessage(prefix + ChatColor.translateAlternateColorCodes('&', no_permission_gr));
			}
		}
	}
}
