package befehle;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.funky33.admintools.Main;
import de.funky33.admintools.MinecraftCmd;

public class sun extends MinecraftCmd {
	public sun() {
		super("sun");
	}
	
	public void doIt(CommandSender sender, Command cmd, String label, String[] args, String prefix) {
		Player player = (Player)sender;
		String no_permission = Main.getInstance().getConfig().getString("system.no_permission");
    	String no_permission_weather = no_permission.replace("[Permission]", "admintools.weather");
    	
		if (cmd.getName().equalsIgnoreCase("sun")) {
			if (player.hasPermission("admintools.weather") | player.hasPermission("admintools.admin")) {
				World world = player.getWorld();
				world.setStorm(false);
				player.sendMessage(prefix + "Weather changed to sun");
			} else {
				player.sendMessage(prefix + ChatColor.translateAlternateColorCodes('&', no_permission_weather));
			}
		}
	}
}
