package befehle;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.funky33.admintools.Main;
import de.funky33.admintools.MinecraftCmd;

public class fly extends MinecraftCmd {
	public fly() {
		super("fly");
	}
	
	@Override
	public void doIt(CommandSender sender, Command cmd, String label, String[] args, String prefix) {
		Player player = (Player)sender;
		String no_permission = Main.getInstance().getConfig().getString("system.no_permission");
    	String no_permission_fly = no_permission.replace("[Permission]", "admintools.fly");
		
		if (cmd.getName().equalsIgnoreCase("fly")) {
			if (player.hasPermission("admintools.fly") | player.hasPermission("admintools.admin")) {
				if (args.length == 1) {
					Player target = Bukkit.getPlayer(args[0]);
					if (target.getAllowFlight() == true) {
						target.setAllowFlight(false);
						target.setFlying(false);
						target.sendMessage(prefix + "Flymode deactivated by " + player.getName());
					} else {
						target.setAllowFlight(true);
						target.setFlying(true);
						target.sendMessage(prefix + "Flymode activated by " + player.getName());
					}
				} else if (args.length == 0) {
					if (player.getAllowFlight() == true) {
						player.setAllowFlight(false);
						player.setFlying(false);
						player.sendMessage(prefix + "Flymode deactivated");
					} else {
						player.setAllowFlight(true);
						player.setFlying(true);
						player.sendMessage(prefix + "Flymode activated");
					}
				}
			} else {
				player.sendMessage(prefix + ChatColor.translateAlternateColorCodes('&', no_permission_fly));
			}
		}
	}
}
